import requests
from libs.ProjectScore import ProjectScore

# 혼자 채점용
# 사용방법 아래 url, name에 본인의 api url과 이름을 넣고 실행 하세요.
if __name__ == '__main__':
    test_user_name = "kyeongrok7"

    url = "your api url"
    name = "your name"
    try:
        ps = ProjectScore(test_user_name)
        ps.hello(url)
        ps.join(url)
        print(name, "score:", ps.score)

    except requests.exceptions.Timeout:
        print(name, "Timeout error:")
    except:
        print(name, "error:", url)



