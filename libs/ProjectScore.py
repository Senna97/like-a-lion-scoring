import requests

class ProjectScore:
    score = 0
    test_user_name = "kyeongrok3"

    def __init__(self, test_user_name):
        self.test_user_name = test_user_name


    def doScore(self):
        self.hello()
        self.join()

    def join(self, url):
        # 회원가입
        r = requests.post(f"{url}/api/v1/users/join",
                          json={"userName":self.test_user_name, "password":"12341234"},
                          timeout=1
                          )
        print(r, r.text)

        # 로그인
        j = r.json()
        if(j['resultCode'] == "SUCCESS" and
                j['result']['userName'] == self.test_user_name):
            self.score += 1

        # 받은 token으로 글쓰기 요청

        # 글 읽기 요청 해서 쓴 글이 잘 등록 되었는지 확인하기

    def hello(self, url):
        score = 0

        # 배포 되었는지 여부

        url = f"{url}/api/v1/hello"
        print(url)
        r = requests.get(url, timeout=1)

        if r.status_code == 200 and r.text == 'hello':
            self.score += 1

        return score
