import requests
from libs.GoogleSpreadsheetUtil import GoogleSpreadsheet
from libs.ProjectScore import ProjectScore


if __name__ == '__main__':
    sheet_url = "https://docs.google.com/spreadsheets/d/1cJ9XQDISo3B6UHzcDmWtG9ucDRDg_YgJPkkW9atCFjk"
    sheet_name = "종합프로젝트"
    test_user_name = "kyeongrok7"
    gs = GoogleSpreadsheet(sheet_url, sheet_name)
    gs.update_student_list('A4:C93')
    infos = gs.read_saved_csv()

    for info in infos:
        if info[2] != "\n":
            url = info[2]
            name = info[1]
            try:
                ps = ProjectScore(test_user_name)
                ps.hello(info[2])
                ps.join(url)
                print(info[1], "score:", ps.score)

            except requests.exceptions.Timeout:
                print(name, "Timeout error:")
            except:
                print(name, "error:", url)



